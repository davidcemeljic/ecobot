# EcoBot

Projekt R EcoBot source

## Getting started

Kako biste mogli otvoriti projekt u Unreal Editoru (najniža verzija: UE5_EA2):
1. Desni klik na EcoBot.uproject - Generate Visual Studio project files. To će kreirati EcoBot.sln.
2. Otvoriti EcoBot.sln pomoću Visual Studia (2019+).
3. Odabrati Build->Build Solution.

Nakon toga EcoBot.uproject će se moći otvoriti u Unreal Editoru.

Verzija EcoBot-a spremna za pokretanje:
[itch.io](https://team134.itch.io/ecobot)
