// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Interactable.h"
#include "PaperTileLayer.h"
#include "PaperTileMapComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "Components/InputComponent.h"
#include "InputCoreTypes.h"
#include "Kismet/GameplayStatics.h"
#include "Paper2DDisplay.generated.h"

UCLASS()
class ECOBOT_API APaper2DDisplay : public APawn, public IInteractable
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APaper2DDisplay();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MapWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MapHeight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 TileWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 TileHeight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FPaperTileInfo> PaperTiles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UPaperTileMapComponent* PaperTileMap;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UCameraComponent* Camera;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USphereComponent* SphereCollision;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void OnConstruction(const FTransform& Transform) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		APawn* PlayerPawn;
	UFUNCTION(BlueprintCallable)
		void RescaleMap();
	


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void Exit();
	virtual void Interact_Implementation(AActor* Caller, FKey InputButton) override;

};
