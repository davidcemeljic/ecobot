// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Interactable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class UInteractable : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class ECOBOT_API IInteractable
{
	GENERATED_BODY()

		// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	//TODO: Caller should be player class
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		FString GetInteractLabelText();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void Interact(AActor* Caller, FKey InputButton);
};
