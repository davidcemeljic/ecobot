// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Paper2DDisplay.h"
#include "Math/RandomStream.h"
#include "PaperTileSet.h"
#include "Lock.h"
#include "PipeMinigame.generated.h"

UCLASS()
class ECOBOT_API APipeMinigame : public APaper2DDisplay
{
	GENERATED_BODY()

public:
	APipeMinigame();

	UPROPERTY(BlueprintReadWrite)
		int32 BlankX;
	UPROPERTY(BlueprintReadWrite)
		int32 BlankY;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 StartX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 StartY;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 EndX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 EndY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* LockedActor;

protected:
	virtual void BeginPlay() override;
	virtual void OnConstruction(const FTransform& Transform) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UPaperTileSet* TileSet;

	UFUNCTION(BlueprintCallable)
		void RenderShuffled(int32 X1, int32 Y1, int32 X2, int32 Y2);
	UFUNCTION(BlueprintCallable)
		FVector2D TileFromLocation(FVector Location);
	UFUNCTION(BlueprintCallable)
		int32 DistanceFromBlankTile(int32 X, int32 Y);
	UFUNCTION(BlueprintCallable)
		void SwapTiles(int32 X1, int32 Y1, int32 X2, int32 Y2);
	UFUNCTION(BlueprintCallable)
		void FillEdges();
	UFUNCTION(BlueprintCallable)
		bool IsSolved();

	UFUNCTION(BlueprintCallable)
		void HandleLMB();
	UPROPERTY()
		bool bHasCreatedWidget;
	UFUNCTION(BlueprintNativeEvent)
		void OnSolved();

private:
	UFUNCTION()
		int8 TileDirection(FPaperTileInfo Tile);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
