// Fill out your copyright notice in the Description page of Project Settings.
#include "Paper2DDisplay.h"

// Sets default values
APaper2DDisplay::APaper2DDisplay()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MapWidth = 4;
	MapHeight = 4;
	TileWidth = 1024;
	TileHeight = 1024;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);

	PaperTileMap = CreateDefaultSubobject<UPaperTileMapComponent>(TEXT("PaperTileMap"));
	PaperTileMap->SetupAttachment(RootComponent);

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(PaperTileMap);
}

// Called when the game starts or when spawned
void APaper2DDisplay::BeginPlay()
{
	Super::BeginPlay();

	/*
	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &APaper2DDisplay::BeginOverlap);
	SphereCollision->OnComponentEndOverlap.AddDynamic(this, &APaper2DDisplay::EndOverlap);
	*/
}

// Called every frame
void APaper2DDisplay::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APaper2DDisplay::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("Interact", IE_Released, this, &APaper2DDisplay::Exit);
	PlayerInputComponent->BindAction("Exit", IE_Released, this, &APaper2DDisplay::Exit);

}

void APaper2DDisplay::OnConstruction(const FTransform& transform) {
	PaperTileMap->CreateNewTileMap(MapWidth, MapHeight, TileWidth, TileHeight, 1.0, true);

	PaperTileMap->SetRelativeLocation(FVector(-13.268906, -3.588931, 122.599174));
	PaperTileMap->SetRelativeScale3D(FVector(0.0086));

	Camera->SetRelativeLocation(FVector(1286.253662, 8216.970703, -1869.28772));
	Camera->SetRelativeRotation(FRotator(0, -90, 0));
	Camera->SetRelativeScale3D(FVector(100));

	SphereCollision->SetRelativeScale3D(FVector(5));

	RescaleMap();
}

void APaper2DDisplay::RescaleMap() {
	float partial = (PaperTileMap->GetRelativeScale3D().X * 4 * 1024) / (FMath::Max(MapWidth, MapHeight) * FMath::Max(TileWidth, TileHeight));
	float locationPartial = (PaperTileMap->GetRelativeScale3D().X * 1024 - partial * FMath::Max(TileWidth, TileHeight)) / 2.0;

	PaperTileMap->SetRelativeLocation(PaperTileMap->GetRelativeLocation() + FVector(locationPartial * -1, 0, locationPartial));
	PaperTileMap->SetRelativeScale3D(FVector(partial));

	float X = MapWidth / 2.0 * TileWidth - TileWidth / 2;
	float Z = MapHeight / 2.0 * TileHeight - TileHeight / 2;

	Camera->SetRelativeLocation(FVector(X, 8216, -Z));
}

void APaper2DDisplay::Exit() {
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	PlayerController->bShowMouseCursor = false;
	PlayerController->Possess(PlayerPawn);
}


void APaper2DDisplay::Interact_Implementation(AActor* Caller, FKey InputButton) {
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	PlayerPawn = PlayerController->GetPawn();
	PlayerController->Possess(this);
	PlayerController->bShowMouseCursor = true;
}
