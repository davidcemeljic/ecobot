// Fill out your copyright notice in the Description page of Project Settings.
#define TILESET "PaperTileSet'/Game/Paper2DDisplay/PipeMinigame/EcoBot_pipes_TileSet.EcoBot_pipes_TileSet'"
#define BLANK_IDX 6
#define UNAVAIL_IDX 7
#define INOUT_IDX 1

#include "PipeMinigame.h"

// Sets default values
APipeMinigame::APipeMinigame()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BlankX = 0;
	BlankY = 0;
	StartX = 1;
	StartY = 1;
	EndX = 4;
	EndY = 4;
	bHasCreatedWidget = false;

	ConstructorHelpers::FObjectFinder<UPaperTileSet> TileSetFinder(TEXT(TILESET));
	if (TileSetFinder.Succeeded()) TileSet = TileSetFinder.Object;
	else UE_LOG(LogTemp, Warning, TEXT("Could not find EcoBot_pipes_TileSet"));

}

// Called when the game starts or when spawned
void APipeMinigame::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APipeMinigame::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APipeMinigame::OnConstruction(const FTransform& transform) {
	Super::OnConstruction(transform);

	RenderShuffled(StartX, StartY, EndX, EndY);
	FillEdges();
}

// Called to bind functionality to input
void APipeMinigame::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("LMB", IE_Released, this, &APipeMinigame::HandleLMB);
}

void APipeMinigame::HandleLMB() {
	if (IsSolved()) return;

	FVector WorldLocation;
	FVector WorldDirection;
	if (UGameplayStatics::GetPlayerController(GetWorld(), 0)->DeprojectMousePositionToWorld(WorldLocation, WorldDirection)) {
		FHitResult HitResult;
		if (GetWorld()->LineTraceSingleByChannel(HitResult, WorldLocation, WorldLocation + WorldDirection * 500, ECC_Visibility)) {
			FVector2D Tile = TileFromLocation(HitResult.Location);
			int32 X = Tile.X;
			int32 Y = Tile.Y;
			if ((X >= StartX && X < EndX + 1) && (Y >= StartY && Y < EndY + 1)) {
				if (DistanceFromBlankTile(X, Y) == 1) {
					SwapTiles(X, Y, BlankX, BlankY);
					BlankX = X;
					BlankY = Y;
				}
			}
		}
	}
}

bool APipeMinigame::IsSolved() {
	int32 X = 1;
	int32 Y = 1;
	int8 Dir = 8;

	while (true)
	{
		if (X == 4 && Y == 5) {
			if (!bHasCreatedWidget) {
				OnSolved();
				bHasCreatedWidget = true;
			}
			return true;
		}
		int8 Dir2 = TileDirection(PaperTileMap->GetTile(X, Y, 0));
		if (Dir & Dir2) {
			Dir = Dir ^ Dir2;
			if (Dir & 8) {
				Dir = 2;
				Y--;
			}
			else if (Dir & 4) {
				Dir = 1;
				X++;
			}
			else if (Dir & 2) {
				Dir = 8;
				Y++;
			}
			else if (Dir & 1) {
				Dir = 4;
				X--;
			}
			continue;
		}
		break;
	}

	return false;
}

int8 APipeMinigame::TileDirection(FPaperTileInfo Tile) {
	switch (Tile.GetTileIndex()) {
	case 0:
		return 5;
		break;
	case 1:
		return 10;
		break;
	case 2:
		return 6;
		break;
	case 3:
		return 3;
		break;
	case 4:
		return 12;
		break;
	case 5:
		return 9;
		break;
	default:
		return 0;
		break;
	}
}


void APipeMinigame::RenderShuffled(int32 X1, int32 Y1, int32 X2, int32 Y2) {

	const int32 NumShuffles = PaperTiles.Num() - 1;
	FRandomStream RandomStream = FRandomStream();
	for (int32 i = 0; i < NumShuffles; ++i) {
		int32 SwapIdx = RandomStream.RandRange(i, NumShuffles);
		PaperTiles.Swap(i, SwapIdx);
	}

	for (int32 i = 0; i < PaperTiles.Num(); i++) {
		const int32 partial = MapWidth * X1 + X1 + i + (MapWidth + X1 - X2 - 1) * (i / (X2 + 1 - X1));
		int32 X = partial % MapWidth;
		int32 Y = partial / MapWidth;
		FPaperTileInfo tile = PaperTiles[i];
		PaperTileMap->SetTile(X, Y, 0, tile);

		if (tile.GetTileIndex() == BLANK_IDX) {
			BlankX = X;
			BlankY = Y;
		}
	}

	PaperTileMap->RebuildCollision();
}

FVector2D APipeMinigame::TileFromLocation(FVector Location) {
	FVector TransformedLocation = Location - PaperTileMap->GetTileCornerPosition(0, 0, 0, true);
	TransformedLocation = TransformedLocation.RotateAngleAxis(PaperTileMap->GetComponentRotation().Yaw, FVector(0, 0, -1));

	int32 X = FMath::TruncToInt(FMath::Lerp(0.f, (float)MapWidth, TransformedLocation.X / ((MapWidth * TileWidth) * PaperTileMap->GetComponentScale().X)));
	int32 Y = FMath::TruncToInt(FMath::Lerp(0.f, (float)MapHeight, -TransformedLocation.Z / ((MapHeight * TileHeight) * PaperTileMap->GetComponentScale().Z)));

	return FVector2D(X, Y);
}

int32 APipeMinigame::DistanceFromBlankTile(int32 X, int32 Y) {
	return FMath::Abs(X - BlankX) + FMath::Abs(Y - BlankY);
}

void APipeMinigame::SwapTiles(int32 X1, int32 Y1, int32 X2, int32 Y2) {
	FPaperTileInfo Tile1 = PaperTileMap->GetTile(X1, Y1, 0);
	FPaperTileInfo Tile2 = PaperTileMap->GetTile(X2, Y2, 0);

	PaperTileMap->SetTile(X1, Y1, 0, Tile2);
	PaperTileMap->SetTile(X2, Y2, 0, Tile1);
}

void APipeMinigame::FillEdges() {
	FPaperTileInfo UnavailableTile = FPaperTileInfo();
	UnavailableTile.TileSet = TileSet;
	UnavailableTile.PackedTileIndex = UNAVAIL_IDX;

	for (int32 i = 0; i < StartX; i++) {
		for (size_t j = 0; j < MapWidth; j++) {
			PaperTileMap->SetTile(i, j, 0, UnavailableTile);
		}
	}

	for (int32 i = EndX + 1; i < MapHeight; i++) {
		for (size_t j = 0; j < MapWidth; j++) {
			PaperTileMap->SetTile(i, j, 0, UnavailableTile);
		}
	}

	for (int32 i = 0; i < MapHeight; i++) {
		for (size_t j = 0; j < StartY; j++) {
			PaperTileMap->SetTile(i, j, 0, UnavailableTile);
		}
	}

	for (int32 i = 0; i < MapWidth; i++) {
		for (size_t j = EndY + 1; j < MapHeight; j++) {
			PaperTileMap->SetTile(i, j, 0, UnavailableTile);
		}
	}

	FPaperTileInfo InOutTile = FPaperTileInfo();
	InOutTile.TileSet = TileSet;
	InOutTile.PackedTileIndex = INOUT_IDX;

	PaperTileMap->SetTile(1, 0, 0, InOutTile);
	PaperTileMap->SetTile(4, 5, 0, InOutTile);
}

void APipeMinigame::OnSolved_Implementation() {
	UE_LOG(LogTemp, Log, TEXT("Solved!"));
}